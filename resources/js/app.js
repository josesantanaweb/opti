window.$ = window.jQuery = require('jquery');
require('./jquery.bxslider.js'); 
require('./jquery.zoom.js'); 
require('./jquery-ui.js'); 

$(document).ready(function(){
    // Slider
    $('.slider-content').bxSlider({
        auto: true,
        autoControls: false,
        stopAutoOnClick: true,
        pager: false,
    });
    // Menu
    $('.menu-toggle').click(function(){
        $('.menu').toggleClass('is-open')
    })
    //Zoom
    $('div.glass-zoom').zoom();
    //Select
    $('.select').on('click','.placeholder',function(){
        var parent = $(this).closest('.select');
        if ( ! parent.hasClass('is-open')){
          parent.addClass('is-open');
          $('.select.is-open').not(parent).removeClass('is-open');
        }else{
          parent.removeClass('is-open');
        }
      }).on('click','ul>li',function(){
        var parent = $(this).closest('.select');
        parent.removeClass('is-open').find('.placeholder').text( $(this).text() );
        parent.find('input[type=hidden]').attr('value', $(this).attr('data-value') );
    });

    // Drag glass
    $( ".glass-move img" ).draggable();

    // Change User
    $('#user-1').click(function(){
      $(this).addClass('is-active');
      $('#user-2').removeClass('is-active');
      $('#user-3').removeClass('is-active');
      $("#userImg").attr('src',"img/user-2.png");
      return false;
    });
    $('#user-2').click(function(){
      $(this).addClass('is-active');
      $('#user-1').removeClass('is-active');
      $('#user-3').removeClass('is-active');
      $("#userImg").attr('src',"img/user-1.png");
      return false;
    });
    $('#user-3').click(function(){
      $(this).addClass('is-active');
      $('#user-1').removeClass('is-active');
      $('#user-2').removeClass('is-active');
      $("#userImg").attr('src',"img/user-3.png");
      return false;
    });
    // Uplaod Imagen
    $("#inputGroupFile01").change(function(event) {  
      RecurFadeIn();
      readURL(this);    
    });
    $("#inputGroupFile01").on('click',function(event){
      RecurFadeIn();
    });
    function readURL(input) {    
      if (input.files && input.files[0]) {   
        var reader = new FileReader();
        var filename = $("#inputGroupFile01").val();
        filename = filename.substring(filename.lastIndexOf('\\')+1);
        reader.onload = function(e) {
          debugger;      
          $('#userImg').attr('src', e.target.result);
          $('#userImg').hide();
          $('#userImg').fadeIn(500);      
          $('.custom-file-label').text(filename);             
        }
        reader.readAsDataURL(input.files[0]);    
      } 
      $(".alert").removeClass("loading").hide();
    }
    function RecurFadeIn(){ 
      console.log('ran');
      FadeInAlert("Wait for it...");  
    }
    function FadeInAlert(text){
      $(".alert").show();
      $(".alert").text(text).addClass("loading");  
    }
});