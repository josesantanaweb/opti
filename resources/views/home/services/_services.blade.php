<!-- Services -->
<section class="services">
    <div class="container">
        <div class="services-item">
            <img src="img/service-men.jpg" alt="description" class="services-image">
            <a href="/catalogue" class="services-title">Lentes de Hombres</a>
            <p class="services-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem, provident!</p>
        </div>
        <div class="services-item is-glass">
            <img src="img/service-women.jpg" alt="description" class="services-image">
            <a  href="/catalogue" class="services-title">Lentes de Mujer</a>
            <p class="services-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem, provident!</p>
        </div>
        <div class="services-item">
            <img src="img/service-bussines.jpg" alt="description" class="services-image">
            <a  href="/catalogue" class="services-title">Servicios de Empresa</a>
            <p class="services-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem, provident!</p>
        </div>
    </div>
</section>