{{-- News --}}
<section class="news">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="news-item news-offert">
                    <div class="news-caption">
                        <h4>Ofertas Especiales</h4>
                        <p>Desde $75</p>
                    </div>
                    <a href="#" class="news-button">Mirar Ahora</a>
                </div>
                <div class="news-item news-shop">
                    <div class="news-caption">
                        <h4>Observa nuestra Tienda</h4>
                    </div>
                    <a href="#" class="news-button">Mirar Ahora</a>
                </div>
            </div>
            <div class="col-md-6">
                <a href="#" class="news-item news-collection">
                    <div class="news-caption">
                        <h4>Nueva Coleccion</h4>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>