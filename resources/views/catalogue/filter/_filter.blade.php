<div class="filter">
    <h3>Filtro de Busqueda</h3>
    <div class="filter-item">
        <h4>Fijar Precio</h4>
        <div class="filter-price">
            <span>
                <input type="text" placeholder="0">
                <label for="">Min</label>
            </span>
            <span>
                <input type="text" placeholder="100">
                <label for="">Max</label>
            </span>
        </div>
        <button href="#" class="button button-primary">Buscar</button>
    </div>
    <div class="filter-item">
        <h4>Condicion</h4>
        <li>Nuevo</li>
        <li>Usado</li>
    </div>
    <div class="filter-item">
        <h4>Marcas</h4>
        <li>Marca #1</li>
        <li>Marca #2</li>
        <li>Marca #3</li>
        <li>Marca #4</li>
    </div>
    <div class="filter-item">
        <h4>Puesto</h4>
        <li>Mas Visitados</li>
        <li>Mas Vendidos</li>
        <li>Segun tu Busqueda</li>
    </div>
    <div class="filter-item">
        <h4>Categoria</h4>
        <li>Anteojos</li>
        <li>Gafas de Sol</li>
        <li>Gafas de Seguridad</li>
        <li>Monturas</li>
    </div>
</div>