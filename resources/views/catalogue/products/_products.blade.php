<!-- Products -->
<section class="products">
    <div class="container">
        @include ('catalogue/filter/_filter')
        @include ('catalogue/gallery/_gallery')
    </div>
</section>