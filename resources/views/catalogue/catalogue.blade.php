@extends('layouts.app')

@section('content')

  @include ('shared/slider/_slider')
  @include ('shared/brands/_brands')
  @include ('catalogue/products/_products')

@endsection