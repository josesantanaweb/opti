<div class="question">
    <div class="container">

        <h3>1. Para que usted utiliza sus vidrios?</h3>
        <div class="question-body">
            <div class="question-item">
                <h4>Vision Unica</h4>
                <p>Distancia</p>
                <span>Incluida</span>
            </div>
            <div class="question-item">
                <h4>Vision Cercana</h4>
                <p>Lectura</p>
                <span>Incluida</span>
            </div>
            <div class="question-item">
                <h4>Progresivo</h4>
                <p>Distancia</p>
                <span>$ 55</span>
            </div>
            <div class="question-item">
                <h4>Bifocal</h4>
                <p>Distancia y lectura</p>
                <span>$ 88</span>
            </div>
            <div class="question-item">
                <h4>Sin Prescripcion</h4>
                <p>Moda</p>
                <span>Incluida</span>
            </div>
        </div>

    </div>
</div>