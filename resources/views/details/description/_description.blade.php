<sidebar class="glass-description">
    <h3>Descripcion del Producto</h3>
    <article>
        <h4>Marcas y Medidas</h4>
        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ab nostrum eaque illo commodi quia explicabo facilis quaerat provident eveniet delectus.</p>
        <ul>
            <li>Detalles</li>
            <li>Talla: <span>pequena</span></li>
            <li>Material: <span>Plastico</span></li>
            <li>Forma: <span>Rectangular</span></li>
            <li>Bisagras de primavera: <span>Si</span></li>
            <li>Color: <span>Negro</span></li>
        </ul>
        <ul>
            <li>Medidas</li>
            <li><img src="img/model-front-y.png">Altura: <span>50mm</span></li>
            <li><img src="img/model-front-x.png">Anchura: <span>50mm</span></li>
            <li><img src="img/model-space.png">Puente: <span>50mm</span></li>
            <li><img src="img/model-profile.png">Templo: <span>50mm</span></li>
        </ul>
    </article>
</sidebar>