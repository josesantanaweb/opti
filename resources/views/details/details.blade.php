@extends('layouts.app')

@section('content')

  @include ('shared/slider/_slider')
  @include ('shared/brands/_brands')
  @include ('details/glass/_glass')
  @include ('shared/offerts/_offerts')
  @include ('details/question/_question')
  @include ('details/prescription/_prescription')

@endsection