<div class="prescription">
    <div class="container">
        <h3>2. Ingrese su prescripcion</h3>
        <div class="prescription-body">
            <div class="prescription-tabs">
                <li class="is-active">LLenalo En linea</li>
                <li>Cargar o enviar mas tarde</li>
                <li>Usar mi receta guardada</li>
            </div>
            <div class="prescription-content">
                <div class="row align-items-lg-center mb-4">
                    <div class="col-lg">
                        <div class="prescription-item">
                            <p class="prescription-label">Derecha <b>(OD)</b></p>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="prescription-item">
                            <p class="prescription-label mb-3">Esfera (SPH)<i class="fa fa-question-circle-o"></i></p>
                            <div class="select">
                                <span class="placeholder">Nada</span>
                                <ul>
                                    <li data-value="en">Option 1</li>
                                    <li data-value="es">Option 2</li>
                                </ul>
                                <input type="hidden" name="changeme"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="prescription-item">
                            <p class="prescription-label mb-3">Cilindro (CYL)<i class="fa fa-question-circle-o"></i></p>
                            <div class="select">
                                <span class="placeholder">Nada</span>
                                <ul>
                                    <li data-value="en">Option 1</li>
                                    <li data-value="es">Option 2</li>
                                </ul>
                                <input type="hidden" name="changeme"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="prescription-item">
                            <p class="prescription-label mb-3">Eje (AXI)<i class="fa fa-question-circle-o"></i></p>
                            <div class="select">
                                <span class="placeholder">Nada</span>
                                <ul>
                                    <li data-value="en">Option 1</li>
                                    <li data-value="es">Option 2</li>
                                </ul>
                                <input type="hidden" name="changeme"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="prescription-item">
                            <p class="prescription-label mb-3">Anadir<i class="fa fa-question-circle-o"></i></p>
                            <div class="select">
                                <span class="placeholder">Nada</span>
                                <ul>
                                    <li data-value="en">Option 1</li>
                                    <li data-value="es">Option 2</li>
                                </ul>
                                <input type="hidden" name="changeme"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row align-items-lg-center mb-4">
                    <div class="col-lg">
                        <div class="prescription-item">
                            <p class="prescription-label">Izquierda <b>(OS)</b></p>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="prescription-item">
                            <div class="select">
                                <span class="placeholder">Nada</span>
                                <ul>
                                    <li data-value="en">Option 1</li>
                                    <li data-value="es">Option 2</li>
                                </ul>
                                <input type="hidden" name="changeme"/>
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-lg">
                        <div class="prescription-item">
                            <div class="select">
                                <span class="placeholder">Nada</span>
                                <ul>
                                    <li data-value="en">Option 1</li>
                                    <li data-value="es">Option 2</li>
                                </ul>
                                <input type="hidden" name="changeme"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="prescription-item">
                            <div class="select">
                                <span class="placeholder">Nada</span>
                                <ul>
                                    <li data-value="en">Option 1</li>
                                    <li data-value="es">Option 2</li>
                                </ul>
                                <input type="hidden" name="changeme"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="prescription-item">
                            <div class="select">
                                <span class="placeholder">Nada</span>
                                <ul>
                                    <li data-value="en">Option 1</li>
                                    <li data-value="es">Option 2</li>
                                </ul>
                                <input type="hidden" name="changeme"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="hr"></div>
                
                <div class="row align-items-lg-center mb-4">
                    <div class="col-lg-2">
                        <div class="prescription-item">
                            <p class="prescription-label">PD<i class="fa fa-question-circle-o"></i></p>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="prescription-item">
                            <div class="select">
                                <span class="placeholder">Nada</span>
                                <ul>
                                    <li data-value="en">Option 1</li>
                                    <li data-value="es">Option 2</li>
                                </ul>
                                <input type="hidden" name="changeme"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="prescription-item flex">
                            <input type="checkbox" name="" class=" mr-2">
                            <p class="prescription-label">Tengo 2 numeros para mi PD</p>
                        </div>
                    </div>
                   
                </div>
                <div class="row align-items-lg-center">
                    <div class="col-lg-2">
                        <div class="prescription-item">
                            <p class="prescription-label">Cerca PD<i class="fa fa-question-circle-o"></i></p>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="prescription-item">
                            <div class="select">
                                <span class="placeholder">Nada</span>
                                <ul>
                                    <li data-value="en">Option 1</li>
                                    <li data-value="es">Option 2</li>
                                </ul>
                                <input type="hidden" name="changeme"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>