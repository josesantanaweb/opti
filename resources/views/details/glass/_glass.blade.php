<section class="glass">
    <div class="container">
        <div class="glass-colors">
            <div class="glass-change">
                <span class="glass-change-left">
                    <div class="glass-profile glass-zoom">
                        <img src="img/glass-thumb1.png" alt="">
                        <p>Frontal</p>
                    </div>
                    <div class="glass-profile glass-zoom">
                        <img src="img/glass-thumb2.png" alt="">
                        <p>Lateral</p>
                    </div>
                </span>
                {{-- <span>
                    <div class="glass-selected glass-zoom">
                        <img src="img/glass-thumb3.png" alt="">
                    </div>
                </span> --}}
                <span>
                    <div class="glass-photo"> 
                        <form id="form1" runat="server"class="flex justify-content-between align-items-center">
                            <div class="glass-user" id='img_contain'>
                                <img src="img/user-1.png" id="userImg">

                                <div class="glass-move">
                                    <img src="img/glass-move.png">
                                </div>
                            </div>
                            <div class="glass-uploadInfo">
                                <p>Sube tu foto o puedes usar uno de nuestros modelos</p>
                                <label class="glass-uplaod" for="inputGroupFile01">
                                    <i class="fa fa-upload"></i>
                                    <label for="">Cargar Imagen</label>
                                </label>
                                <input type="file" id="inputGroupFile01" class="imgInp custom-file-input">
                            </div>
                        </form>
                        <div class="glass-users">
                            <div class="glass-users-item" id="user-1">
                                <img src="img/user-2.png" id="blah">
                            </div>
                            <div class="glass-users-item" id="user-2">
                                <img src="img/user-1.png">
                            </div>
                            <div class="glass-users-item" id="user-3">
                                <img src="img/user-3.png">
                            </div>
                        </div>
                    </div>
                </span>
            </div>
            <div class="glass-controls">
                <span class="glass-trial"><i class="fa fa-camera"></i>Pruebatelo</span>
                <span>
                    {{-- <div class="glass-palette">
                        <span class="black"></span>
                        <span class="pink"></span>
                        <span class="blue"></span>
                        <span class="turquese"></span>
                        <span class="violet"></span>
                        <span class="blue"></span>
                        <span class="yellow"></span>
                        <span class="green"></span>
                    </div> --}}
                    {{-- <h4>Lentes de Calidad hechos de buen material y lo tenenmos en oferta</h4>
                    <p>25% de descuento</p> --}}
                </span>
            </div>
            <div class="glass-cartInfo">
                <li>
                    <div class="glass-code">
                        <h4>#44444 <span>25</span></h4>
                    </div>
                    <div class="glass-votes">
                        <span>
                            <i class="fa fa-star is-active"></i>
                            <i class="fa fa-star is-active"></i>
                            <i class="fa fa-star is-active"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </span>
                        <p>125 Votos Positivos</p>
                    </div>
                </li>
                <li>
                    <div class="glass-price">
                        <p>Precio</p>
                        <h4>$95.00</h4>
                    </div>
                    <a href="#" class="button button-primary">Añadir al carrito</a>
                </li>
            </div>  
        </div>
        @include ('details/description/_description')
    </div>
</section>