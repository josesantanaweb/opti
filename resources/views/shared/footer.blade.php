<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md">
                <ul class="footer-list">
                    <li><h3>Menu</h3></li>
                    <li><a href="#">Anteojos</a></li>
                    <li><a href="#">Gafas de sol</a></li>
                    <li><a href="#">Multifocal</a></li>
                    <li><a href="#">Marcas</a></li>
                    <li><a href="#">Contacto</a></li>
                </ul>
            </div>
            <div class="col-md">
                <ul class="footer-list">
                    <li><h3>Marcas</h3></li>
                    <li><a href="#">Marca #1</a></li>
                    <li><a href="#">Marca #2</a></li>
                    <li><a href="#">Marca #3</a></li>
                    <li><a href="#">Marca #4</a></li>
                    <li><a href="#">Marca #5</a></li>
                </ul>
            </div>
            <div class="col-md">
                <ul class="footer-list">
                    <li><h3>Sobre Nosotros</h3></li>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fuga fugit dolor architecto dolore omnis voluptates.</p>
                </ul>
            </div>
            <div class="col-md">
                <ul class="footer-list">
                    <li>Contacto</li>
                    <li><a href="#">correa@gmail.com</a></li>
                    <li><a href="#">+58 (212) 000 0000</a></li>
                    <li><a href="#">+58 (283) 000 0000</a></li>
                </ul>
            </div>
            <div class="col-md">
                <!-- Search bar -->
                <div class="search">
                    <input type="text" placeholder="Buscar..">
                    <i class="fa fa-search"></i>
                </div>
            </div>
        </div>
    </div>
</footer>