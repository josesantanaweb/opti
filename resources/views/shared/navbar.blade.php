<!-- Header -->
<header>
    <!-- Top Menu -->
    <div class="top">
        <div class="container">
        <ul class="top-menu">
            <li><a href="#">Ayuda</a></li>
            <li><a href="#">Mi Cuenta</a></li>
            <li><a href="#">Ordenes</a></li>
            <li><a href="#"><i class="fa fa-heart-o"></i></a></li>
            <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
        </ul>
        </div>
    </div>
    <!-- Navbar -->
    <div class="navbar">
        <div class="container">
        <a href="/" class="logo">
            <img src="img/logo.png" alt="">
        </a>
        <span class="fa fa-bars menu-toggle"></span>
        <!-- Menu -->
        <nav class="menu">
            <li><a href="#">Lentes</a></li>
            <li><a href="#">Gafas de Sol</a></li>
            <li><a href="#">Multifocal</a></li>
            <li><a href="#">Marcas</a></li>
            <li><a href="#">Contactos</a></li>
            <li>
            <!-- Search bar -->
            <div class="search">
                <input type="text" placeholder="Buscar..">
                <i class="fa fa-search"></i>
            </div>
            </li>
        </nav>
        </div>
    </div>
</header>