<!-- Slider -->
<div class="slider">
    <div class="slider-caption">
        <h3>Bienvenido a Opti Austral</h3>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor tempora mollitia vel alias, quis sequi commodi perspiciatis necessitatibus. Veniam, recusandae?</p>
        <div class="slider-group">
        <a href="#" class="slider-button">Hacer Pedido</a>
        <a href="#" class="slider-button slider-button-outline">Contactanos</a>
    </div>
    </div>
    <div class="slider-content">
        <div class="d-none d-sm-block"><img src="img/banner-men.png" alt=""></div>
        <div class="d-none d-sm-block"><img src="img/banner-women.jpg" alt=""></div>
        <div class="d-block d-sm-none"><img src="img/banner-men-movil.png" alt=""></div>
        <div class="d-block d-sm-none"><img src="img/banner-women-movil.jpg" alt=""></div>
    </div>
</div>
