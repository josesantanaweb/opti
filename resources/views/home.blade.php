@extends('layouts.app')

@section('content')

  @include ('shared/slider/_slider')
  @include ('home/services/_services')
  @include ('home/location/_location')
  @include ('shared/brands/_brands')
  @include ('home/news/_news')
  @include ('shared/offerts/_offerts')

@endsection